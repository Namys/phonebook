package com.getjavajob.web06.phoneBook.model.impl;

import com.getjavajob.web06.phoneBook.model.Base;

import javax.persistence.*;
import java.util.List;

/**
 * Created by ����� on 05.12.2015.
 */
@Entity
@Table(name = "departments")
public class Department extends Base {
    private String name;
    @OneToOne
    @JoinColumn(name = "chief_id")
    private Employee chief;
    @OneToMany(mappedBy = "department",cascade = CascadeType.ALL)
    private List<Address> addresses;
    @OneToMany(mappedBy = "department",cascade = CascadeType.ALL)
    private List<Phone> phones;

    public Department() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Employee getChief() {
        return chief;
    }

    public void setChief(Employee chief) {
        this.chief = chief;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @Override
    public String toString() {
        int chief = this.chief == null ? 0 : this.chief.getId();
        return "com.getjavajob.web06.phoneBook.model.impl.Department{" +
                "name='" + name + '\'' +
                ", chief=" + chief +
//                ", address=" + address +
                "} " + super.toString();
    }
}
