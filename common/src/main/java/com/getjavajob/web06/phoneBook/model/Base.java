package com.getjavajob.web06.phoneBook.model;


import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

/**
 * Created by ����� on 05.12.2015.
 */
@MappedSuperclass
public abstract class Base {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Base(int id) {
        this.id = id;
    }

    public Base() {
    }

    @Override
    public String toString() {
        return "{" +
                "id=" + id +
                '}';
    }
}
