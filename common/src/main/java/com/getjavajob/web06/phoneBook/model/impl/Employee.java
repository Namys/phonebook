package com.getjavajob.web06.phoneBook.model.impl;

import com.getjavajob.web06.phoneBook.model.Base;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * Created by ����� on 05.12.2015.
 */
@Entity
@Table(name = "employees")
public class Employee extends Base implements Serializable {
    private String surname;
    private String name;
    private String family_name;
    private String email;
    private Date bornDate;
    private String icq;
    private String skype;
    private String other;
    @OneToMany(mappedBy = "employee",cascade = CascadeType.ALL)
    private List<Phone> phones;
    @OneToMany(mappedBy = "employee",cascade = CascadeType.ALL)
    private List<Address> addresses;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "department_id")
    private Department department;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "chief_id")
    private Employee chief;
    @Column(name = "img_blob")
    byte[] img;

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFamily_name() {
        return family_name;
    }

    public void setFamily_name(String family_name) {
        this.family_name = family_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Date getBornDate() {
        return bornDate;
    }

    public void setBornDate(Date bornDate) {
        this.bornDate = bornDate;
    }

    public String getIcq() {
        return icq;
    }

    public void setIcq(String icq) {
        this.icq = icq;
    }

    public String getSkype() {
        return skype;
    }

    public void setSkype(String skype) {
        this.skype = skype;
    }

    public String getOther() {
        return other;
    }

    public void setOther(String other) {
        this.other = other;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    public Employee getChief() {
        return chief;
    }

    public void setChief(Employee chief) {
        this.chief = chief;
    }

    public byte[] getImg() {
        return img;
    }

    public void setImg(byte[] img) {
        this.img = img;
    }

    public List<Address> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address> addresses) {
        this.addresses = addresses;
    }

    public List<Phone> getPhones() {
        return phones;
    }

    public void setPhones(List<Phone> phones) {
        this.phones = phones;
    }

    @Override
    public String toString() {
        String department = this.department == null ? "null" : this.department.getName();
        Integer chief = this.chief == null ? 0 : this.chief.getId();
        return "com.getjavajob.web06.phoneBook.model.impl.Employee{" +
                "surname='" + surname + '\'' +
                ", name='" + this.name + '\'' +
                ", family_name='" + family_name + '\'' +
                ", email='" + email + '\'' +
                ", bornDate=" + bornDate +
                ", icq='" + icq + '\'' +
                ", skype='" + skype + '\'' +
                ", other='" + other + '\'' +
                ", department=" + department +
                ", chief=" + chief +
                ", img=" + img +
                ", phones=" + phones +
                ", addresses=" + addresses +
                "} " + super.toString();
    }

}

