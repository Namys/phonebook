package com.getjavajob.web06.phoneBook.model.impl;

import com.getjavajob.web06.phoneBook.model.Base;

import javax.persistence.*;

/**
 * Created by ����� on 15.12.2015.
 */
@Entity
@Table
public class Address extends Base {
    private String country;
    private String city;
    private String street;
    private String house;
    private String room;
    private String address_index;
    @ManyToOne
    @JoinColumn(name = "employee_id")
    private Employee employee;
    private boolean isHome;
    @ManyToOne
    @JoinColumn(name = "department_id")
    private Department department;

    public Address() {
    }

    public boolean isHome() {
        return isHome;
    }

    public void setIsHome(boolean isHome) {
        this.isHome = isHome;
    }

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHouse() {
        return house;
    }

    public void setHouse(String house) {
        this.house = house;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public String getAddress_index() {
        return address_index;
    }

    public void setAddress_index(String address_index) {
        this.address_index = address_index;
    }

    public Employee getEmployee() {
        return employee;
    }

    public void setEmployee(Employee employee) {
        this.employee = employee;
    }

    public Department getDepartment() {
        return department;
    }

    public void setDepartment(Department department) {
        this.department = department;
    }

    @Override
    public String toString() {
        int employee_id = employee == null ? 0 : employee.getId();
        int department_id = department == null ? 0 : department.getId();
        return "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", house='" + house + '\'' +
                ", room='" + room + '\'' +
                ", address_index='" + address_index + '\'' +
                ", employee_id=" + employee_id +
                ", isHome=" + isHome() +
                ", department_id=" + department_id ;
    }
}
