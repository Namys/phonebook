package com.getjavajob.web06.phoneBook.service;

import com.getjavajob.web06.phoneBook.dao.impl.AddressDao;
import com.getjavajob.web06.phoneBook.dao.impl.EmployeeDao;
import com.getjavajob.web06.phoneBook.dao.impl.PhoneDao;
import com.getjavajob.web06.phoneBook.model.impl.Address;
import com.getjavajob.web06.phoneBook.model.impl.Employee;
import com.getjavajob.web06.phoneBook.model.impl.Phone;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ����� on 13.03.2016.
 */
@Service
public class EmployeeService {
    private EmployeeDao employeeDao;
    private PhoneDao phoneDao;
    private AddressDao addressDao;

    @Autowired
    public EmployeeService(EmployeeDao employeeDao, PhoneDao phoneDao, AddressDao addressDao) {
        this.employeeDao = employeeDao;
        this.phoneDao = phoneDao;
        this.addressDao = addressDao;
    }

    @Transactional
    public void save(Employee entity) {
        update(entity);
    }

    private void update(Employee entity) {
        employeeDao.update(entity);
        /*List<Phone> phonesFromBase = phoneDao.getAllByEmployeeID(entity.getId());
        List<Address> addressesFromBase = addressDao.getAllByEmployeeID(entity.getId());
        Set<Integer> updatedPhones = new HashSet<>();
        Set<Integer> updatedAddresses = new HashSet<>();*/
        /*List<Phone> phones = entity.getPhones();
        if (phones!=null) {
            for (Phone phone : phones) {
                if (phone.getId() != 0) {
                    phoneDao.update(phone);
                    updatedPhones.add(phone.getId());
                    updatedPhones.add(phone.getId());
                } else {
                    phoneDao.add(phone);
                }
            }
        }
        List<Address> addresses = entity.getAddresses();
        if (addresses!=null) {
            for (Address address : addresses) {
                if (address.getId() != 0) {
                    addressDao.update(address);
                    updatedAddresses.add(address.getId());
                } else {
                    addressDao.add(address);
                }
            }
        }*/
        /*for (Phone phone : phonesFromBase) {
            if (updatedPhones.add(phone.getId())) {
                phoneDao.delete(phone.getId());
            }
        }
        for (Address dbAddress : addressesFromBase) {
            if (updatedAddresses.add(dbAddress.getId())) {
                addressDao.delete(dbAddress.getId());
            }
        }*/
    }

    private void add(Employee entity) {
        employeeDao.add(entity);
        /*List<Phone> phones = entity.getPhones();
        for (Phone phone : phones) {
            phoneDao.add(phone);
        }
        List<Address> addresses = entity.getAddresses();
        for (Address address : addresses) {
            addressDao.add(address);
        }*/
    }

    @Transactional
    public void remove(int id) {
        employeeDao.delete(id);
    }

    public Employee get(int id) {
        /*employee.setPhones(phoneDao.getAllByEmployeeID(id));
        employee.setAddresses(addressDao.getAllByEmployeeID(id));*/
        return employeeDao.get(id);
    }


    public List<Employee> getAll() {
        /*List<Employee> employeeList = employeeDao.getAll();
        List<Phone> phoneList = phoneDao.getAll();
        Map<Integer, Employee> employeeMap = new HashMap<>();
        for (Employee employee : employeeList) {
            employeeMap.put(employee.getId(), employee);
        }
        for (Phone phone : phoneList) {
            employeeMap.get(phone.getEmployee_id()).getPhones().add(phone);
        }*/
        return employeeDao.getAll();
    }

    public List<Address> getAllAddresses() {
        return addressDao.getAll();
    }

    public List<Phone> getAllPhones() {
        return phoneDao.getAll();
    }
    public List<Employee> getDiapason(int offset, int limit) {
        return employeeDao.getDiapason(offset, limit);
    }
    public List<Employee> getDiapasonByID(int start, int end) {
        return employeeDao.getDiapason(start,end);
    }

    public int getMaxID() {
        return employeeDao.getMaxID();
    }

    public Long getSize() {
        return employeeDao.getSize();
    }
}
