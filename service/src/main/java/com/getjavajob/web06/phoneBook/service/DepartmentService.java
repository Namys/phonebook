package com.getjavajob.web06.phoneBook.service;

import com.getjavajob.web06.phoneBook.dao.impl.DepartmentDao;
import com.getjavajob.web06.phoneBook.model.impl.Department;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by ����� on 27.04.2016.
 */
@Service
public class DepartmentService {
    @Autowired
    private DepartmentDao departmentDao;
    @Transactional
    public void save(Department entity) {
        departmentDao.add(entity);
    }
    @Transactional
    public void remove(int id) {
        departmentDao.delete(id);
    }
    public Department get(int id) {
        return departmentDao.get(id);
    }
    public List<Department> getAll() {
        return departmentDao.getAll();
    }
}
