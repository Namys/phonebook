import com.getjavajob.web06.phoneBook.dao.impl.DepartmentDao;
import com.getjavajob.web06.phoneBook.model.impl.Department;
import com.getjavajob.web06.phoneBook.service.DepartmentService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by ����� on 21.05.2016.
 */
@RunWith(MockitoJUnitRunner.class)
public class DepartmentServiceTest {
    @InjectMocks
    private DepartmentService departmentService;
    @Mock
    private DepartmentDao departmentDAO;


    @Test
    public void saveTest() {
        Department dep = new Department();
        departmentService.save(dep);
        when(departmentService.get(1)).thenReturn(dep);
        verify(departmentDAO).add(dep);
    }

    @Test
    public void getTest() {
        Department dep = new Department();
        when(departmentService.get(1)).thenReturn(dep);
        departmentService.get(1);
        verify(departmentDAO).get(1);

    }

    @Test
    public void getAllTest() {
        Department dep = new Department();
        Department dep1 = new Department();
        List<Department> excepted = new ArrayList<>(Arrays.asList(dep,dep1));
        when(departmentService.getAll()).thenReturn(excepted);
        departmentService.getAll();
        verify(departmentDAO).getAll();
    }

    @Test
    public void removeTest() {
        Department dep = new Department();
        dep.setId(1);
        departmentService.remove(dep.getId());
        verify(departmentDAO).delete(dep.getId());
    }
}
