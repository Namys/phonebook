import com.getjavajob.web06.phoneBook.dao.impl.EmployeeDao;
import com.getjavajob.web06.phoneBook.model.impl.Employee;
import com.getjavajob.web06.phoneBook.service.EmployeeService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
/**
 * Created by ����� on 21.05.2016.
 */

@RunWith(MockitoJUnitRunner.class)
public class EmployeeServiceTest {
    @InjectMocks
    private EmployeeService employeeService;
    @Mock
    private EmployeeDao employeeDAO;


    @Test
    public void saveTest() {
        Employee emp = new Employee();
        employeeService.save(emp);
        when(employeeService.get(1)).thenReturn(emp);
        verify(employeeDAO).update(emp);
    }

    @Test
    public void getTest() {
        Employee emp = new Employee();
        when(employeeService.get(1)).thenReturn(emp);
        employeeService.get(1);
        verify(employeeDAO).get(1);

    }

    @Test
    public void getAllTest() {
        Employee emp = new Employee();
        Employee emp1 = new Employee();
        List<Employee> excepted = new ArrayList<>(Arrays.asList(emp,emp1));
        when(employeeService.getAll()).thenReturn(excepted);
        employeeService.getAll();
        verify(employeeDAO).getAll();
    }

    @Test
    public void removeTest() {
        Employee emp = new Employee();
        emp.setId(1);
        employeeService.remove(emp.getId());
        verify(employeeDAO).delete(emp.getId());
    }
}
