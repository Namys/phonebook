# PhoneBook

** Functionality: **

+ registration

+ ajax loading of cities and countries

+ authentication

+ ajax search with pagination

+ display profile

+ edit profile

+ edit profile privacy settings

+ upload and download avatar

+ users export to xml 

** Tools: ** 

JDK 7, Spring 4, JPA 2 / Hibernate 5, XStream, jQuery 1, Twitter Bootstrap 3, JUnit 4, Mockito, Maven 3, Git / Bitbucket, Tomcat 8, PostgreSQL, IntelliJIDEA 14. 

** Notes: ** 
SQL ddl is located in the `dao\src\main\resources\phonebook.sql`

** Screenshots: **

Show employees page

[http://www.getjavajob.com](http://www.getjavajob.com)
![b70090c2494fbe4bb590e98a7ae22d54.png](https://bitbucket.org/repo/aLKGpg/images/1122908627-b70090c2494fbe4bb590e98a7ae22d54.png)

Edit employee page

![441631ef4f42d527f202fce9f926ea02.png](https://bitbucket.org/repo/aLKGpg/images/3249128239-441631ef4f42d527f202fce9f926ea02.png)

Login page

![1ddf654e6d1cfee4a5c94960c89ec18f.png](https://bitbucket.org/repo/aLKGpg/images/2137865049-1ddf654e6d1cfee4a5c94960c89ec18f.png)

­­**Букаев Намыс** Тренинг getJavaJob,
Training getJavaJob, Dec 2015 - April 2016
http://www.getjavajob.com