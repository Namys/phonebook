DROP SCHEMA IF EXISTS `test`;

CREATE SCHEMA IF NOT EXISTS `test`;

USE `test`;

CREATE TABLE IF NOT EXISTS `address` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `country` varchar(100) DEFAULT NULL,
  `city` varchar(100) NOT NULL,
  `street` varchar(100) DEFAULT NULL,
  `house` varchar(20) DEFAULT NULL,
  `room` varchar(20) DEFAULT NULL,
  `address_index` varchar(20) DEFAULT NULL,
  `employee_id` int(11) DEFAULT '0',
  `isHome` char(5) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `departments` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `chief_id` int(11) DEFAULT NULL,
  `name` char(200) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `employees` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `surname` varchar(50) DEFAULT NULL,
  `name` varchar(50) NOT NULL,
  `family_name` varchar(20) DEFAULT NULL,
  `email` varchar(40) DEFAULT NULL,
  `bornDate` date DEFAULT NULL,
  `icq` varchar(20) DEFAULT NULL,
  `skype` varchar(20) DEFAULT NULL,
  `other` text,
  `department_id` int(11) DEFAULT NULL,
  `chief_id` int(11) DEFAULT NULL,
  `img_blob` longblob,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `phone` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `number` varchar(25) NOT NULL,
  `employee_id` int(11) DEFAULT NULL,
  `department_id` int(11) DEFAULT NULL,
  `isHome` char(5) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=utf8;
