import com.getjavajob.web06.phoneBook.dao.impl.AddressDao;
import com.getjavajob.web06.phoneBook.model.impl.Address;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ����� on 22.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context-override.xml"})
@Transactional
public class AddressDaoTest {
    @Autowired
    private AddressDao addressDao;

    @Test
    public void getByID(){
        Address address=new Address();
        address.setCity("city");
        addressDao.add(address);
        Assert.assertNotNull(addressDao.get(address.getId()).getCity());
    }
    @Test
    public void add() {
        Address address=new Address();
        address.setCity("city");
        addressDao.add(address);
        Assert.assertNotNull(addressDao.get(address.getId()));
    }

    @Test
    public void update() {
        Address address=new Address();
        address.setCity("city");
        addressDao.add(address);
        address.setCity("city1");
        addressDao.update(address);
        Assert.assertEquals("city1", addressDao.get(address.getId()).getCity());
    }

    @Test
    public void getAll() {
        Address address=new Address();
        address.setCity("city");
        Address address1=new Address();
        address1.setCity("city1");
        addressDao.add(address);
        addressDao.add(address1);
        Assert.assertEquals(2, addressDao.getAll().size());
    }

    @Test
    public void delete() {
        Address address=new Address();
        address.setCity("city");
        addressDao.add(address);
        addressDao.delete(address.getId());
        Assert.assertNull(addressDao.get(address.getId()));
    }

}
