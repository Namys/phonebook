import com.getjavajob.web06.phoneBook.dao.impl.DepartmentDao;
import com.getjavajob.web06.phoneBook.model.impl.Department;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ����� on 05.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context-override.xml"})
@Transactional
public class DepartmentDaoTest {
    @Autowired
    private DepartmentDao departmentDao;

    @Test
    public void getByID(){
        Department department=new Department();
        department.setName("dep");
        departmentDao.add(department);
        Assert.assertNotNull(departmentDao.get(department.getId()));
    }
    @Test
    public void add() {
        Department department=new Department();
        department.setName("dep");
        departmentDao.add(department);
        Assert.assertNotNull(departmentDao.get(department.getId()));
    }

    @Test
    public void update() {
        Department department=new Department();
        department.setName("dep");
        departmentDao.add(department);
        department.setName("dep1");
        departmentDao.update(department);
        Assert.assertEquals("dep1", departmentDao.get(department.getId()).getName());
    }

    @Test
    public void getAll() {
        Department department=new Department();
        department.setName("dep");
        Department department1=new Department();
        department1.setName("dep1");
        departmentDao.add(department);
        departmentDao.add(department1);
        Assert.assertEquals(2, departmentDao.getAll().size());
    }

    @Test
    public void delete() {
        Department department=new Department();
        department.setName("dep");
        departmentDao.add(department);
        departmentDao.delete(department.getId());
        Assert.assertNull(departmentDao.get(department.getId()));
    }
}
