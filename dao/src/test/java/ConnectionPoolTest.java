import com.getjavajob.web06.phoneBook.dao.connection.ConnectionPoolNew;
import org.junit.Assert;
import org.junit.Test;

import java.sql.Connection;

/**
 * Created by ����� on 05.12.2015.
 */
public class ConnectionPoolTest {
    ConnectionPoolNew connectionPool=ConnectionPoolNew.getInstance();
    @Test
    public void getDifferentConnection() {
        Connection connection = connectionPool.getConnection();
        connectionPool.returnConnection();
        Connection connection1=connectionPool.getConnection();
        Assert.assertNotEquals(connection, connection1);
    }
    @Test
    public void getEqualConnection() {
        Connection connection = connectionPool.getConnection();
        Connection connection1=connectionPool.getConnection();
        Assert.assertEquals(connection, connection1);
    }
}
