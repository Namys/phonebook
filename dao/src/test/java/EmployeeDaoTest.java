import com.getjavajob.web06.phoneBook.dao.impl.EmployeeDao;
import com.getjavajob.web06.phoneBook.model.impl.Employee;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ����� on 22.12.2015.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context-override.xml"})
@Transactional
public class EmployeeDaoTest {
    @Autowired
    private EmployeeDao employeeDao;

    @Test
    public void getDiapasonByID(){
        Employee employee=new Employee();
        employee.setName("emp");
        Employee employee1=new Employee();
        employee1.setName("emp1");
        Employee employee2=new Employee();
        employee2.setName("emp2");
        Employee employee3=new Employee();
        employee3.setName("emp3");
        Employee employee4=new Employee();
        employee4.setName("emp4");
        employeeDao.add(employee);
        employeeDao.add(employee1);
        employeeDao.add(employee2);
        employeeDao.add(employee3);
        employeeDao.add(employee4);
        Assert.assertEquals(3,employeeDao.getDiapasonByID(employee1.getId(), employee3.getId()).size());
    }
    @Test
    public void getDiapason(){
        Employee employee=new Employee();
        employee.setName("emp");
        Employee employee1=new Employee();
        employee1.setName("emp1");
        Employee employee2=new Employee();
        employee2.setName("emp2");
        Employee employee3=new Employee();
        employee3.setName("emp3");
        Employee employee4=new Employee();
        employee4.setName("emp4");
        employeeDao.add(employee);
        employeeDao.add(employee1);
        employeeDao.add(employee2);
        employeeDao.add(employee3);
        employeeDao.add(employee4);
        Assert.assertEquals(3,employeeDao.getDiapason(0,3).size());
    }


    @Test
    public void getByID(){
        Employee employee=new Employee();
        employee.setName("emp");
        employeeDao.add(employee);
        Assert.assertNotNull(employeeDao.get(employee.getId()));
    }

    @Test
    public void add() {
        Employee employee=new Employee();
        employee.setName("emp");
        employeeDao.add(employee);
        Assert.assertNotNull(employeeDao.get(employee.getId()));
    }

    @Test
    public void update() {
        Employee employee=new Employee();
        employee.setName("emp");
        employeeDao.add(employee);
        employee.setName("emp1");
        employeeDao.update(employee);
        Assert.assertEquals("emp1", employeeDao.get(employee.getId()).getName());
    }

    @Test
    public void getAll() {
        Employee employee=new Employee();
        employee.setName("emp");
        Employee employee1=new Employee();
        employee1.setName("emp1");
        employeeDao.add(employee);
        employeeDao.add(employee1);
        Assert.assertEquals(2, employeeDao.getAll().size());
        Assert.assertEquals(2, employeeDao.getAll().size());
    }
    @Test
    public void getSize() {
        Employee employee=new Employee();
        employee.setName("emp");
        Employee employee1=new Employee();
        employee1.setName("emp1");
        Employee employee2=new Employee();
        employee2.setName("emp2");
        employeeDao.add(employee);
        employeeDao.add(employee1);
        employeeDao.add(employee2);
        Assert.assertEquals(3, employeeDao.getAll().size());
        Assert.assertEquals("3", employeeDao.getSize().toString());
    }

    @Test
    public void delete() {
        Employee employee=new Employee();
        employee.setName("emp");
        employeeDao.add(employee);
        employeeDao.delete(employee.getId());
        Assert.assertNull(employeeDao.get(employee.getId()));
    }
}
