import com.getjavajob.web06.phoneBook.dao.impl.PhoneDao;
import com.getjavajob.web06.phoneBook.model.impl.Phone;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by ����� on 31.12.2015.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:dao-context-override.xml"})
@Transactional
public class PhoneDaoTest {

    @Autowired
    private PhoneDao phoneDao;

    @Test
    public void getByID(){
        Phone phone=new Phone();
        phone.setNumber("1");
        phoneDao.add(phone);
        Assert.assertNotNull(phoneDao.get(phone.getId()));
    }
    @Test
    public void add() {
        Phone phone=new Phone();
        phone.setNumber("1");
        phoneDao.add(phone);
        Assert.assertNotNull(phoneDao.get(phone.getId()));
    }

    @Test
    public void update() {
        Phone phone=new Phone();
        phone.setNumber("1");
        phoneDao.add(phone);
        phone.setNumber("2");
        phoneDao.update(phone);
        Assert.assertEquals("2", phoneDao.get(phone.getId()).getNumber());
    }

    @Test
    public void getAll() {
        Phone phone=new Phone();
        phone.setNumber("1");
        Phone phone1=new Phone();
        phone1.setNumber("2");
        phoneDao.add(phone);
        phoneDao.add(phone1);
        Assert.assertEquals(2, phoneDao.getAll().size());
    }

    @Test
    public void delete() {
        Phone phone=new Phone();
        phone.setNumber("1");
        phoneDao.add(phone);
        phoneDao.delete(phone.getId());
        Assert.assertNull(phoneDao.get(phone.getId()));
    }
}
