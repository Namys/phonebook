package com.getjavajob.web06.phoneBook.dao.impl;

import com.getjavajob.web06.phoneBook.dao.HibernateGenericDao;
import com.getjavajob.web06.phoneBook.model.impl.Employee;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;
import java.util.List;

/**
 * Created by ����� on 05.12.2015.
 */
@Repository
public class EmployeeDao extends HibernateGenericDao<Employee> {
/*    private static final String TABLE_NAME = "employees";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME + " (" +
            "id," +
            "surname,\n" +
            "name,\n" +
            "family_name,\n" +
            "email,\n" +
            "bornDate,\n" +
            "icq,\n" +
            "skype,\n" +
            "other,\n" +
            "department_id,\n" +
            "chief_id) VALUES (?,?,?,?,?,?,?,?,?,?,?)";
    private static final String UPDATE_BY_ID = "UPDATE " + TABLE_NAME + " SET " +
            "surname = ? \n," +
            "name = ? \n," +
            "family_name = ? \n," +
            "email = ? \n," +
            "bornDate = ? \n," +
            "icq = ? \n," +
            "skype = ? \n," +
            "other = ? \n," +
            "department_id = ? \n," +
            "chief_id  = ? " +
            "WHERE id = ?";

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getInsertStatement() {
        return INSERT;
    }

    @Override
    protected String getUpdateByIdStatement() {
        return UPDATE_BY_ID;
    }

    @Override
    protected Employee createInstanceFromResult(ResultSet resultSet) throws SQLException, ParseException {
        int id = resultSet.getInt("id");
        if (id == 0) {
            return null;
        }
        Employee employee = new Employee();
        employee.setId(id);
        employee.setSurname(resultSet.getString("surname"));
        employee.setName(resultSet.getString("name"));
        employee.setFamily_name(resultSet.getString("family_name"));//todo �� ������ ���� ���� ����� ������������
        employee.setEmail(resultSet.getString("email"));
        employee.setBornDate(resultSet.getDate("bornDate"));
        employee.setIcq(resultSet.getString("icq"));
        employee.setSkype(resultSet.getString("skype"));
        employee.setOther(resultSet.getString("other"));
        employee.setPhones(new ArrayList<Phone>());
        int chief_id = resultSet.getInt("chief_id");
        if (chief_id > 0) {
            Employee chief = get(chief_id);
            employee.setChief(chief);
        } else {
            employee.setChief(new Employee());
        }
        return employee;
    }

    @Override
    protected void addAtr(Employee entity, PreparedStatement preparedStatement) throws SQLException {
        System.out.println(entity);
        preparedStatement.setString(2, entity.getSurname());
        preparedStatement.setString(3, entity.getName());
        preparedStatement.setString(4, entity.getFamily_name());
        preparedStatement.setString(5, entity.getEmail());
        preparedStatement.setDate(6, (Date) entity.getBornDate());
        preparedStatement.setString(7, entity.getIcq());
        preparedStatement.setString(8, entity.getSkype());
        preparedStatement.setString(9, entity.getOther());
        preparedStatement.setInt(10, entity.getDepartment().getId());
        preparedStatement.setInt(11, entity.getChief().getId());
    }

    @Override
    protected void updateAtr(Employee entity, Class<?> clazz, Field[] fields, List<Object> values) {
        super.updateAtr(entity, clazz, fields, values);
        values.add(entity.getDepartment().getId());
        Employee chief = entity.getChief();
        if (chief != null)
            values.add(chief.getId());
        else
            values.add(null);
    }*/

    public List<Employee> getDiapasonByID(int start, int end) {
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<Employee> query = criteriaBuilder.createQuery(Employee.class);
        Root<Employee> root = query.from(Employee.class);
        EntityType<Employee> rootModel = root.getModel();
        query.select(root).where(criteriaBuilder.between(root.get(rootModel.getSingularAttribute("id",Integer.class)),start,end));
        return manager.createQuery(query).getResultList();
    }
    public List<Employee> getDiapason(int offset, int limit) {
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<Employee> query = criteriaBuilder.createQuery(Employee.class);
        return manager.createQuery(query.select(query.from(Employee.class))).setFirstResult(offset).setMaxResults(limit).getResultList();
    }
}
