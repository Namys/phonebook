package com.getjavajob.web06.phoneBook.dao.impl;

import com.getjavajob.web06.phoneBook.dao.HibernateGenericDao;
import com.getjavajob.web06.phoneBook.model.Base;

import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

/**
 * Created by ����� on 15.05.2016.
 */
public abstract class ManyGenericDao<T extends Base> extends HibernateGenericDao<T> {
    public List<T> getAllByEmployeeID(int id) {
        CriteriaBuilder criteriaBuilder= manager.getCriteriaBuilder();
        CriteriaQuery<T> query=criteriaBuilder.createQuery(type);
        Root<T> from = query.from(type);
        CriteriaQuery<T> selectQuery=query.select(from).where(criteriaBuilder.equal(from.get("employee_id"), id));
        return manager.createQuery(selectQuery).getResultList();
    }
}
