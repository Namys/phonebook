package com.getjavajob.web06.phoneBook.dao;

import com.getjavajob.web06.phoneBook.model.Base;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import javax.persistence.metamodel.EntityType;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * Created by ����� on 15.05.2016.
 */

public class HibernateGenericDao<T extends Base> implements CrudDao<T> {
    @Autowired
    protected EntityManager manager;
    protected Class<T> type;

    public HibernateGenericDao() {
        Type t = getClass().getGenericSuperclass();
        ParameterizedType pt = (ParameterizedType) t;
        type = (Class) pt.getActualTypeArguments()[0];
    }

    @Override
    public void add(T base) {
        base.setId(manager.merge(base).getId());
    }

    @Override
    public void update(T base) {
        manager.merge(base);
    }

    @Override
    public void delete(int id) {
        manager.remove(manager.find(type, id));
    }

    @Override
    public T get(int id) {
        return manager.find(type, id);
    }

    @Override
    public List<T> getAll() {
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<T> query = criteriaBuilder.createQuery(type);
        CriteriaQuery<T> selectQuery = query.select(query.from(type));
        return manager.createQuery(selectQuery).getResultList();
    }

    public int getMaxID() {
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<Integer> query = criteriaBuilder.createQuery(Integer.class);
        Root<T> root = query.from(type);
        EntityType<T> rootModel = root.getModel();
        query.select(criteriaBuilder.max(root.get(rootModel.getSingularAttribute("id",Integer.class))));
        return manager.createQuery( query ).getSingleResult();
    }
    public Long getSize(){
        CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
        CriteriaQuery<Long> query = criteriaBuilder.createQuery(Long.class);
        Root<T> root = query.from(type);
        query.select(criteriaBuilder.count((root.get("id"))));
        return manager.createQuery( query ).getSingleResult();
    }
}
