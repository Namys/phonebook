package com.getjavajob.web06.phoneBook.dao.impl;

import com.getjavajob.web06.phoneBook.model.impl.Address;
import org.springframework.stereotype.Repository;

/**
 * Created by ����� on 16.12.2015.
 */
@Repository
public class AddressDao extends ManyGenericDao<Address> {
    /*private static final String TABLE_NAME = "address";
    private static final String INSERT = "INSERT INTO " + TABLE_NAME + " (" +
            "id,country,city,street,house,room,address_index,employee_id,isHome,departament_id) VALUES (?,?,?,?,?,?,?,?,?,?)";
    private static final String UPDATE_BY_ID = "UPDATE " + TABLE_NAME + " SET " +
            "country = ?, city = ?, street = ?, house = ?, room = ?, address_index = ?, employee_id = ?, isHome  = ?, departament_id  = ? "+
            "WHERE id = ?";

    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    protected String getInsertStatement() {
        return INSERT;
    }

    @Override
    protected String getUpdateByIdStatement() {
        return UPDATE_BY_ID;
    }

    @Override
    protected Address createInstanceFromResult(ResultSet resultSet) throws SQLException, ParseException {
        Address address=new Address();
        address.setId(resultSet.getInt("id"));
        address.setCountry(resultSet.getString("country"));
        address.setCity(resultSet.getString("city"));
        address.setStreet(resultSet.getString("street"));
        address.setHouse(resultSet.getString("house"));
        address.setRoom(resultSet.getString("room"));
        address.setAddress_index(resultSet.getString("address_index"));
        address.setIsHome("1".equals(resultSet.getString("isHome")));
        address.setEmployee_id(resultSet.getInt("employee_id"));
        address.setDepartament_id(resultSet.getInt("departament_id"));
        return address;
    }

    @Override
    protected void addAtr(Address entity, PreparedStatement preparedStatement) throws SQLException {

    }
    public List<Address> getAllByEmployeeID(int employee_id) {
        List<Address> where = getWhere(getSelectAllStatement() + " WHERE employee_id=" + employee_id);
        System.out.println("where:"+where);
        return where;
    }*/

}
