package com.getjavajob.web06.phoneBook.dao.connection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * Created by ����� on 13.03.2016.
 */
public class ConnectionPoolNew {
    private static ConnectionPoolNew instance;
    private BlockingQueue<Connection> connections;
    private ThreadLocal<Connection> currentConnection=new ThreadLocal<>();

    private ConnectionPoolNew(){
        Properties properties = new Properties();
        try {
            properties.load(ConnectionPoolNew.class.getClassLoader().getResourceAsStream("connection.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Class.forName(properties.getProperty("driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String dbURL = properties.getProperty("url");;
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        int maxConnections = Integer.parseInt(properties.getProperty("maxConnections"));
        connections= new ArrayBlockingQueue<>(maxConnections);
        for (int i = 0; i < maxConnections; i++) {
            try {
                Connection connection = DriverManager.getConnection(dbURL, user, password);
                connections.put(connection);
            } catch (SQLException e) {
                e.printStackTrace();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
    public static synchronized ConnectionPoolNew getInstance(){
        if (instance==null){
            instance=new ConnectionPoolNew();
        }
        return instance;
    }

    public Connection getConnection() {
        if (currentConnection.get()==null){
            try {
                currentConnection.set(connections.take());
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return currentConnection.get();
    }

    public void returnConnection() {
        try {
            connections.put(currentConnection.get());
            currentConnection.remove();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void closeConnectionPool() {
        for (Connection connection:connections){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
