package com.getjavajob.web06.phoneBook.dao;

import com.getjavajob.web06.phoneBook.model.Base;
import com.getjavajob.web06.phoneBook.model.impl.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ����� on 05.12.2015.
 */
public abstract class GenericDao<T extends Base> implements CrudDao<T> {
    protected abstract String getTableName();

    protected abstract String getInsertStatement();

    protected abstract String getUpdateByIdStatement();

    protected abstract T createInstanceFromResult(ResultSet resultSet) throws SQLException, ParseException;

    @Autowired
    protected JdbcTemplate jdbcTemplate;

    private RowMapper<T> rowMapper = new RowMapper<T>() {
        @Override
        public T mapRow(ResultSet rs, int rowNum) {
            try {
                return createInstanceFromResult(rs);
            } catch (ParseException | SQLException e) {
                e.printStackTrace();
            }
            return null;
        }
    };

    @Override
    public void add(final T entity) {
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            public PreparedStatement createPreparedStatement(Connection connection) throws SQLException {
                PreparedStatement preparedStatement = connection.prepareStatement(getInsertStatement());
                addAtr(entity, preparedStatement);
                return preparedStatement;
            }
        }, keyHolder);
//        LOG.debug("inserted availability id = {}.", keyHolder.getKey());
        entity.setId(keyHolder.getKey().intValue());
/*//        try {
//            Connection connection = ds.getConnection();
            Class<?> clazz = entity.getClass();
            int length = clazz.getDeclaredFields().length;
            Field[] fields = new Field[length + 1];
            fields[0] = clazz.getSuperclass().getDeclaredFields()[0];
            System.arraycopy(clazz.getDeclaredFields(), 0, fields, 1, length);
//            try (PreparedStatement prepareStatement = connection.prepareStatement(getInsertStatement())) {
                for (int i = 0; i < fields.length; i++) {
                    Method getMethod = null;
                    Object fieldValue;
                    try {
                        getMethod = clazz.getMethod("get" + capitalizeFirstLetter(fields[i].getName()));
                    } catch (NoSuchMethodException e) {
                        e.printStackTrace();
                    }
                    Object newValue = null;
                    try {
                        if (getMethod != null) {
                            fieldValue = getMethod.invoke(entity);
                            if (!fields[i].getType().isPrimitive() && !(fields[i].getType() == String.class)
                                    && !(fields[i].getType().isEnum()) && !(fields[i].getType() == Date.class) && !(fieldValue == null)) {
                                newValue = ((Base) fieldValue).getId();
                            } else if (fields[i].getType().isEnum()) {
                                newValue = ((java.lang.Enum) fieldValue).ordinal() + 1;
                            } else {
                                newValue = fieldValue;
                            }
                        }
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        e.printStackTrace();
                    }
                    if (newValue instanceof Base) {
                        continue;
                    }
//                    prepareStatement.setObject(i + 1, newValue);

                }
//                prepareStatement.executeUpdate();
            *//*} catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*//*
        entity.setId(getMaxId());*/
    }

    protected abstract void addAtr(T entity, PreparedStatement preparedStatement) throws SQLException;


    @Override
    public void update(T entity) {
        int id = entity.getId();
        Class<?> clazz = entity.getClass();
        Field[] fields = clazz.getDeclaredFields();
        List<Object> values = new ArrayList<>();
        updateAtr(entity, clazz, fields, values);
        values.add(id);
        if (entity instanceof Address){
            System.out.println(values.set(8, null));
        }
        jdbcTemplate.update(getUpdateByIdStatement(), values.toArray());
    }

    protected void updateAtr(T entity, Class<?> clazz, Field[] fields, List<Object> values) {
        for (Field field : fields) {
            Method getMethod = null;
            boolean aBoolean = field.getType().toString().equals("boolean");
            try {
                if (aBoolean) {
                    getMethod = clazz.getMethod(field.getName());
                } else {
                    getMethod = clazz.getMethod("get" + capitalizeFirstLetter(field.getName()));
                }
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
            try {
                if (getMethod != null) {
                    Object fieldValue = getMethod.invoke(entity);
                    Object newValue = null;
                    if (field.getType().isPrimitive() || (field.getType() == String.class || field.getType() == java.util.Date.class)) {
                        newValue = fieldValue;
                    } else if (field.getType().isEnum()) {
                        newValue = ((Enum) fieldValue).ordinal() + 1;
                    }
                    if (newValue != null) {
                        values.add(newValue);
                    }
                }
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
        }
    }

    /*private void updateField(List<Object> values, int id) {
        try {
            Connection connection = ds.getConnection();
            int fieldsQty = values.size();
            try (PreparedStatement prepareStatement = connection.prepareStatement(getUpdateByIdStatement())) {
                for (int i = 0; i < values.size(); i++) {
                    prepareStatement.setObject(i + 1, values.get(i));
                }
                prepareStatement.setObject(fieldsQty + 1, id);
                prepareStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }*/

    @Override
    public void delete(int id) {
        jdbcTemplate.update(getDeleteByIdStatement(), id);
        /*try {
            Connection connection = ds.getConnection();
            try (PreparedStatement prepareStatement = connection.prepareStatement(getDeleteByIdStatement())) {
                prepareStatement.setInt(1, id);
                prepareStatement.executeUpdate();
                connection.commit();
            } catch (SQLException e) {
                e.printStackTrace();
            } finally {
                try {
                    connection.rollback();
                } catch (SQLException e1) {
                    e1.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public T get(int id) {
        /*try {
            Connection connection = ds.getConnection();
            try (PreparedStatement prepareStatement = connection.prepareStatement(getSelectByIdStatement())) {
                prepareStatement.setInt(1, id);
                try (ResultSet resultSet = prepareStatement.executeQuery()) {
                    if (resultSet.next()) {
                        return createInstanceFromResult(resultSet);
                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
        return null;*/
        if (id == 0) {
            return null;
        }
        T t = null;
        try {
            t = jdbcTemplate.queryForObject(getSelectByIdStatement(), new Object[]{id}, rowMapper);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return t;
    }

    public List<T> getAll() throws SQLException {
        return getWhere(getSelectAllStatement());
    }

    protected List<T> getWhere(String selectAllStatement) {
        /*Connection connection = null;
        List<T> resultList = new ArrayList<>();
        try {
            connection = jdbcTemplate.getDataSource().getConnection();
            System.out.println(connection);
            Statement statement = connection.createStatement();
            System.out.println("get state");
            ResultSet resultSet = statement.executeQuery(getSelectAllStatement());
            System.out.println("get set");
            while (resultSet.next()) {
                try {
                    resultList.add(createInstanceFromResult(resultSet));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
            statement.close();
            System.out.println("close state");
            resultSet.close();
            System.out.println("close set");
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            try {
                if (connection != null) {
                    connection.close();
                    System.out.println("connection closed");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return resultList;*/
        List<T> tList = null;
        try {
            tList = jdbcTemplate.query(selectAllStatement, rowMapper);
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return tList;
    }

    public int getMaxId() {
        String sql = "SELECT MAX(id) FROM " + getTableName();
        Integer max = 0;
        try {
            max = jdbcTemplate.queryForObject(sql, new RowMapper<Integer>() {
                @Override
                public Integer mapRow(ResultSet rs, int rowNum) {
                    try {
                        if (rs.next()) {
                            return rs.getInt(1);
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                    return 0;
                }
            });
        } catch (DataAccessException e) {
            e.printStackTrace();
        }
        return max;
    }

    protected String getSelectAllStatement() {
        return "SELECT * FROM " + getTableName();
    }

    protected String getSelectByIdStatement() {
        return getSelectAllStatement() + " WHERE id = ?";
    }

    protected String getDeleteByIdStatement() {
        return "DELETE FROM " + getTableName() + " WHERE id = ?";
    }

    private String capitalizeFirstLetter(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }
}
