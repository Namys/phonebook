package com.getjavajob.web06.phoneBook.dao;

/**
 * Created by ����� on 05.12.2015.
 */

import com.getjavajob.web06.phoneBook.model.Base;

import java.sql.SQLException;
import java.util.List;

public interface CrudDao<T extends Base> {

    void add(T base);

    void update(T base);

    void delete(int id);

    T get(int id);

    List<T> getAll() throws SQLException;

}
