package com.getjavajob.web06.phoneBook.dao.connection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Map;
import java.util.Properties;

/**
 * Created by ����� on 21.12.2015.
 */

public class ConnectionPoolOld {
    private Hashtable<Connection, Boolean> connections;
    private int increment;
    private String dbURL, user, password;
    private ThreadLocal<Connection> connectionThreadLocal;

    public ConnectionPoolOld() throws ClassNotFoundException{

        Properties properties = new Properties();
        try {
            properties.load(ConnectionPoolOld.class.getClassLoader().getResourceAsStream("connection.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        Class.forName(properties.getProperty("driver"));
        this.dbURL = properties.getProperty("url");;
        this.user = properties.getProperty("user");
        this.password = properties.getProperty("password");
        this.increment = Integer.parseInt(properties.getProperty("maxConnections"));
        connections = new Hashtable<Connection, Boolean>();
        for (int i = 0; i < Integer.parseInt(properties.getProperty("initConnections")); i++) {
            try {
                connections.put(DriverManager.getConnection(dbURL, user, password), Boolean.FALSE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    public Connection getConnection() {
        Enumeration<Connection> cons = connections.keys();
        synchronized (connections) {
            while (cons.hasMoreElements()) {
                Connection con = cons.nextElement();
                Boolean b = connections.get(con);
                if (!b) {
                    connections.put(con, Boolean.TRUE);
                    return con;
                }
            }
        }
        for (int i = 0; i < increment; i++) {
            try {
                connections.put(DriverManager.getConnection(dbURL, user, password), Boolean.FALSE);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return getConnection();
    }

    public void returnConnection(Connection returned) {
        Connection con;
        Enumeration cons = connections.keys();
        while (cons.hasMoreElements()) {
            con = (Connection) cons.nextElement();
            if (con == returned) {
                connections.put(con, Boolean.FALSE);
                break;
            }
        }
    }

    public void closeConnectionPool() {
        if (connections.size() > 0) {
            for (Map.Entry<Connection, Boolean> conBoolEntry : connections.entrySet()) {
                try {
                    Connection key = conBoolEntry.getKey();
                    if (key != null) {

                        key.close();
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}