package com.getjavajob.web06.phoneBook.dao.connection;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * Created by ����� on 21.12.2015.
 */
public class ConnectionPool {
    private static ConnectionPool instance;
    private ArrayBlockingQueue<Connection> connections;
    private ThreadLocal<Connection> currentConnection=new ThreadLocal<>();

    private ConnectionPool(){
        Properties properties = new Properties();
        try {
            properties.load(ConnectionPool.class.getClassLoader().getResourceAsStream("connection.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            Class.forName(properties.getProperty("driver"));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        String dbURL = properties.getProperty("url");;
        String user = properties.getProperty("user");
        String password = properties.getProperty("password");
        int maxConnections = Integer.parseInt(properties.getProperty("maxConnections"));
        connections = new ArrayBlockingQueue<>(maxConnections);
        for (int i = 0; i < maxConnections; i++) {
            try {
                connections.put(DriverManager.getConnection(dbURL, user, password));
            } catch (InterruptedException | SQLException e) {
                e.printStackTrace();
            }
        }
    }
    public static synchronized ConnectionPool getInstance(){
        if (instance==null){
            instance=new ConnectionPool();
        }
        return instance;
    }

    public Connection getConnection() {
        if (currentConnection.get()==null){
            Connection poll = connections.poll();
            currentConnection.set(poll);
            return poll;
        }
        Connection connection = currentConnection.get();
        if (connections.contains(connection)){
           connections.remove(connection);
            return connection;
        }
        return getConnection();
    }

    public void returnConnection() {
        connections.add(currentConnection.get());
    }

    public void closeConnectionPool() {
        for (Connection connection:connections){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}