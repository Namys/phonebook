CREATE SCHEMA IF NOT EXISTS test;

USE test;

CREATE TABLE `address` (
  `id`            INT(11)      NOT NULL AUTO_INCREMENT,
  `country`       VARCHAR(100) NOT NULL,
  `city`          VARCHAR(100) NOT NULL,
  `street`        VARCHAR(100) NOT NULL,
  `house`         VARCHAR(20)           DEFAULT NULL,
  `room`          VARCHAR(20)           DEFAULT NULL,
  `address_index` VARCHAR(20)           DEFAULT NULL,
  `employee_id`   INT(11)               DEFAULT '0',
  `isHome`        CHAR(1)               DEFAULT NULL,
  `department_id` INT(11)               DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `departament_id` (`department_id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `departament_id` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`),
  CONSTRAINT `employee_id` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
);
CREATE TABLE `departments` (
  `id`       INT(11) NOT NULL AUTO_INCREMENT,
  `chief_id` INT(11)          DEFAULT NULL,
  `name`     CHAR(200)        DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_name` (`name`),
  KEY `department_chief` (`chief_id`),
  CONSTRAINT `department_chief` FOREIGN KEY (`chief_id`) REFERENCES `employees` (`id`)
);

CREATE TABLE `employees` (
  `id`            INT(11)     NOT NULL AUTO_INCREMENT,
  `surname`       VARCHAR(50)          DEFAULT NULL,
  `name`          VARCHAR(50)          DEFAULT NULL,
  `family_name`   VARCHAR(20) NOT NULL,
  `email`         VARCHAR(40) NOT NULL,
  `bornDate`      DATE        NOT NULL,
  `icq`           VARCHAR(20) NOT NULL,
  `skype`         VARCHAR(20) NOT NULL,
  `other`         TEXT,
  `department_id` INT(11)              DEFAULT NULL,
  `chief_id`      INT(11)              DEFAULT NULL,
  `img_blob`      LONGBLOB,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  UNIQUE KEY `unique_email` (`email`),
  KEY `employee_to_Department` (`department_id`),
  KEY `employee_chief` (`chief_id`),
  CONSTRAINT `employee_chief` FOREIGN KEY (`chief_id`) REFERENCES `employees` (`id`),
  CONSTRAINT `employee_to_Department` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`)
);
CREATE TABLE `phone` (
  `id`            INT(11) NOT NULL AUTO_INCREMENT,
  `number`        VARCHAR(25)      DEFAULT NULL,
  `employee_id`   INT(11)          DEFAULT NULL,
  `department_id` INT(11)          DEFAULT NULL,
  `isHome`        CHAR(1)          DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `unique_id` (`id`),
  KEY `employee_phone` (`employee_id`),
  CONSTRAINT `employee_phone` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
);