package com.getjavajob.web06.phoneBook.ui;


import com.getjavajob.web06.phoneBook.dao.impl.EmployeeDao;
import com.getjavajob.web06.phoneBook.model.impl.Employee;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class HelloWorldServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;
    private EmployeeDao dao;

    @Override
    public void init() throws ServletException {
        WebApplicationContext webApplicationContext = WebApplicationContextUtils.getWebApplicationContext(getServletContext());
        this.dao=webApplicationContext.getBean(EmployeeDao.class);
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException, ServletException {
        List<Employee> employeeList = new ArrayList<>();
        try {
            employeeList = dao.getAll();
        }catch (Exception e){
        }
        StringBuilder line=new StringBuilder("<html><head></head><body>");
        for (Employee employee:employeeList){
            line.append("<div>"+employee.getSurname());
        }
        line.append("</body></html>");
        resp.getOutputStream().write(line.toString().getBytes());
    }


}