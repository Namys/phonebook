package com.getjavajob.web06.phoneBook.ui;

import com.getjavajob.web06.phoneBook.model.impl.Employee;
import com.getjavajob.web06.phoneBook.service.EmployeeService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.ModelAndView;

import java.io.IOException;

/**
 * Created by ����� on 26.04.2016.
 */
@Controller
public class FileUploadController {
    @Autowired
    EmployeeService employeeService;
    @RequestMapping(value = "/show", method = RequestMethod.GET)
    public ModelAndView displayForm(@RequestParam("id") int id) {
        ModelAndView modelAndView=new ModelAndView("file_upload_form");
        modelAndView.addObject("img", Base64.encode(employeeService.get(id).getImg()));
        modelAndView.addObject("id",id);
        return modelAndView;
    }
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String save(@ModelAttribute("uploadForm") FileUploadForm uploadForm,MultipartHttpServletRequest request){
        MultipartFile multipartFile = uploadForm.getFile();
        int id = Integer.parseInt(request.getParameter("id"));
        if (multipartFile != null) {
            Employee employee = employeeService.get(id);
            byte[] bytes = null;
            try {
                bytes = multipartFile.getBytes();
            } catch (IOException e) {
                e.printStackTrace();
            }
            employee.setImg(bytes);
            employeeService.save(employee);
        }
        return "redirect:/update?id="+id;
    }
}
