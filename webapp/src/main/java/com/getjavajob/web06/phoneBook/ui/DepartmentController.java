package com.getjavajob.web06.phoneBook.ui;

import com.getjavajob.web06.phoneBook.model.impl.Address;
import com.getjavajob.web06.phoneBook.model.impl.Department;
import com.getjavajob.web06.phoneBook.model.impl.Phone;
import com.getjavajob.web06.phoneBook.service.DepartmentService;
import com.getjavajob.web06.phoneBook.service.EmployeeService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by ����� on 12.07.2016.
 */
@Controller
public class DepartmentController {
    private final Logger logger= LoggerFactory.getLogger(getClass());
    @Autowired
    private DepartmentService departmentService;
    @Autowired
    private EmployeeService employeeService;

    @RequestMapping(value = "showDepartments",method = RequestMethod.GET)
    public ModelAndView showDepartments(){
        ModelAndView view =new ModelAndView("showDepartments");
        view.addObject("departments",departmentService.getAll());
        logger.info("showDepartments done");
        return view;
    }

    @RequestMapping(value = "showDepartment",method = RequestMethod.GET)
    public ModelAndView showDepartment(@RequestParam("id") int id){
        ModelAndView view =new ModelAndView("showDepartment");
        view.addObject("department",departmentService.get(id));
        logger.info("showDepartment with id=%s done",id);
        return view;
    }

    @RequestMapping(value = "updateDepartment",method = RequestMethod.GET)
    public ModelAndView updateDepartment(@RequestParam("id") int id){
        ModelAndView view =new ModelAndView("showDepartment");
        view.addObject("department",departmentService.get(id));
        view.addObject("employees",employeeService.getAll());
        logger.info("update Department with id=%s",id);
        return view;
    }

    @RequestMapping(value = "doUpdateDepartment",method = RequestMethod.POST)
    public String doUpdateDepartment(@ModelAttribute("departmentForm")Department department,@RequestParam("chief_id") int chief_id){
        logger.debug(department.toString());
        if (chief_id != 0){
            department.setChief(employeeService.get(chief_id));
        }
        if (department.getPhones()!=null){
            for (Phone phone:department.getPhones()){
                phone.setDepartment(department);
                phone.setIsHome(false);
            }
        }
        if (department.getAddresses()!=null){
            for (Address address:department.getAddresses()){
                address.setDepartment(department);
                address.setIsHome(false);
            }
        }
        try {
            departmentService.save(department);
        }catch (Exception e){
            logger.error("update failed. " + e.toString());
            return "redirect:/showDepartments";
        }
        logger.info("Department with id=%s updated",department.getId());
        return "redirect:/showDepartments";
    }
    @RequestMapping(value = "addDepartment",method = RequestMethod.GET)
    public ModelAndView addDepartment(){
        ModelAndView view =new ModelAndView("showDepartment");
        view.addObject("employees",employeeService.getAll());
        logger.info("do addDepartment");
        return view;
    }
}
