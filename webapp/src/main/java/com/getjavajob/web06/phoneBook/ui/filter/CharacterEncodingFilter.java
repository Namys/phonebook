package com.getjavajob.web06.phoneBook.ui.filter;

import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServletResponseWrapper;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * Created by ����� on 31.12.2015.
 */

public class CharacterEncodingFilter implements Filter {

    private String encoding;

    @Override
    public void doFilter(ServletRequest request, ServletResponse response,
                         FilterChain chain) throws IOException, ServletException {
        if (this.encoding != null) {
            System.out.println("Encode your request: " + this.encoding);
            request.setCharacterEncoding(this.encoding);
            response.setCharacterEncoding(this.encoding);
        }
        chain.doFilter(request, new HttpServletResponseWrapper((HttpServletResponse) response) {
            @Override
            public PrintWriter getWriter() throws IOException {
                return new PrintWriter(super.getWriter()) {
                    @Override
                    public void write(String s) {
                        super.write(s.trim());
                    }
                };
            }
        });

    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        this.encoding = filterConfig.getInitParameter("encoding");
    }

    @Override
    public void destroy() {
    }
}