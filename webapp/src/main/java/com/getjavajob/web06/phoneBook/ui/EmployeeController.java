package com.getjavajob.web06.phoneBook.ui;

import com.getjavajob.web06.phoneBook.model.impl.Address;
import com.getjavajob.web06.phoneBook.model.impl.Employee;
import com.getjavajob.web06.phoneBook.model.impl.Phone;
import com.getjavajob.web06.phoneBook.service.DepartmentService;
import com.getjavajob.web06.phoneBook.service.EmployeeService;
import com.sun.org.apache.xerces.internal.impl.dv.util.Base64;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.collections4.Predicate;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by ����� on 22.04.2016.
 */
@Controller
public class EmployeeController {
    private final Logger logger= LoggerFactory.getLogger(getClass());
    @Autowired
    private EmployeeService employeeService;
    @Autowired
    private DepartmentService departmentService;

    @RequestMapping(value = "/showEmployees", method = RequestMethod.GET)
    public ModelAndView showEmployees() {
        List<Employee> employeeList = employeeService.getAll();
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("employees", employeeList);
        return modelAndView;
    }
    @RequestMapping(value = "/testLOG", method = RequestMethod.GET)
    public void testLOG() {
        for (int i=1;i<1000;i++){
            logger.error(String.valueOf(i));
        }
    }

    @RequestMapping(value = "/pageEmployeesByID", method = RequestMethod.GET)
    public ModelAndView showEmployeesByID(@RequestParam("start") int start,@RequestParam("end") int end) {
        List<Employee> employeeList = employeeService.getDiapasonByID(start, end);
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("paginationID",true);
        modelAndView.addObject("employees", employeeList);
        int max=employeeService.getMaxID();
        int pageNumber = end/10;
        if (pageNumber<1){
            pageNumber=1;
        }else if (pageNumber>=max/10+1){
            modelAndView.addObject("maxPage", true);
        }
        modelAndView.addObject("pageNumber", pageNumber);
        return modelAndView;
    }
    @RequestMapping(value = "/pageEmployees", method = RequestMethod.GET)
    public ModelAndView showEmployees(@RequestParam("offset") int offset,@RequestParam("limit") int limit) {
        List<Employee> employeeList = employeeService.getDiapason(offset, limit);
        ModelAndView modelAndView = new ModelAndView("index");
        modelAndView.addObject("pagination",true);
        modelAndView.addObject("employees", employeeList);
        modelAndView.addObject("limit",limit);
        int pageNumber = offset/limit+1;
        Long max=employeeService.getSize();
        if (pageNumber<1){
            pageNumber=0;
        }else if (pageNumber>= max/limit+1){
            modelAndView.addObject("maxPage", true);
        }
        modelAndView.addObject("pageNumber", pageNumber);
        return modelAndView;
    }

    @RequestMapping(value = "/showEmployee", method = RequestMethod.GET)
    public ModelAndView showEmployee(@RequestParam("id") int id) {
        ModelAndView modelAndView = new ModelAndView("updateEmployee");
        Employee employee = employeeService.get(id);
        modelAndView.addObject("employee", employee);
        byte[] img = employee.getImg();
        if (img!=null) {
            modelAndView.addObject("img", Base64.encode(img));
        }
        else {
            String DEFAULT_IMG = "";
            modelAndView.addObject("img", DEFAULT_IMG);
        }
        modelAndView.addObject("show",true);
        logger.info("show employee with id=%s", id);
        return modelAndView;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public ModelAndView showUpdate(@RequestParam("id") int id) {
        Employee employee = employeeService.get(id);
        ModelAndView modelAndView = new ModelAndView("updateEmployee");
        logger.debug(employee.toString());
        byte[] img = employee.getImg();
        if (img!=null) {
            modelAndView.addObject("img", Base64.encode(img));
        }
        else {
            String DEFAULT_IMG = "";
            modelAndView.addObject("img", DEFAULT_IMG);
        }
        modelAndView.addObject("employee", employee);
        modelAndView.addObject("departments", departmentService.getAll());
        modelAndView.addObject("employees", employeeService.getAll());
        modelAndView.addObject("addresses", employeeService.getAllAddresses());
        modelAndView.addObject("phones", employeeService.getAllPhones());
        logger.info("update form for employee with id=%s",id);
        return modelAndView;
    }
    @RequestMapping(value = "/doUpdate", method = RequestMethod.POST)
    public String showUpdate( @ModelAttribute("employeeForm") Employee employee, @RequestParam("department_id") int departament_id, @RequestParam("chief_id") int chief_id) {
        Employee oldEmployee = employeeService.get(employee.getId());
        employee.setImg(oldEmployee.getImg());
        if (departament_id != 0){
            employee.setDepartment(departmentService.get(departament_id));
        }
        if (chief_id != 0) {
            employee.setChief(employeeService.get(chief_id));
        }
        else {
            employee.setChief(null);
        }
        if (employee.getPhones()!=null)
        for (Phone phone:employee.getPhones()){
            phone.setEmployee(employee);
        }
        logger.debug(employee.getAddresses().toString());
        if (employee.getAddresses()!=null)
        for (Address address :employee.getAddresses()){
            address.setEmployee(employee);
        }
        try {
            employeeService.save(employee);
        }catch (Exception e){
            logger.error("update failed. "+e.toString());
            return "redirect:/showEmployees";
        }
        logger.info("employee with id=%s updated",employee.getId());
        return "redirect:/showEmployees";
    }


    @RequestMapping(value = "/removeEmployee")
    public String delEmp(@RequestParam("employee_id") int id) {
        try {
            employeeService.remove(id);
        }catch (Exception e){
            logger.error("removeEmployee failed. "+e.toString());
            return "redirect:/showEmployees";
        }
        logger.info("employee with id=%s updated",id);
        return "redirect:/showEmployees";
    }

    @RequestMapping(value = "/addEmployee", method = RequestMethod.GET)
    public ModelAndView addEmployee() {
        ModelAndView modelAndView = new ModelAndView("updateEmployee");
        modelAndView.addObject("departments", departmentService.getAll());
        modelAndView.addObject("employees", employeeService.getAll());
        logger.info("add form for employee");
        return modelAndView;
    }

    @RequestMapping(value = "/getEmployees", method = RequestMethod.POST)
    @ResponseBody
    public Map<Integer, String> getEmployees(final @RequestParam("filter") String filter) {
        List<Employee> emps = employeeService.getAll();
        CollectionUtils.filter(emps, new Predicate<Employee>() {
            @Override
            public boolean evaluate(Employee employee) {
                return employee.getSurname().toLowerCase().contains(filter.toLowerCase()) || employee.getName().toLowerCase().contains(filter.toLowerCase());
            }
        });
        Map<Integer, String> employees = new HashMap<>();
        for (Employee employee : emps) {
            employees.put(employee.getId(), employee.getSurname() + " " + employee.getName() + " " + employee.getFamily_name());
        }
        return employees;
    }

}
