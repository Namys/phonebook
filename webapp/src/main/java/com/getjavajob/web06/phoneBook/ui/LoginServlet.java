package com.getjavajob.web06.phoneBook.ui;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Properties;

@Controller
public class LoginServlet {

    @Resource(name = "usersProperties")
    private Properties properties;

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public ModelAndView login() {
        ModelAndView modelAndView = new ModelAndView("login");
        return modelAndView;
    }

    @RequestMapping(value = "/doLogin", method = RequestMethod.POST)
    public String doLogin(@RequestParam("email") String email, @RequestParam("password") String password, @RequestParam(name = "remember", required = false) Boolean remember, HttpSession session, HttpServletResponse response) {
        String pass = properties.getProperty(email);
        if (pass != null && password.equals(pass)) {
            session.setAttribute("login", email+":"+pass);
            if (remember != null) {
                Cookie cookie = new Cookie("login", email+":"+pass);
                cookie.setMaxAge(Integer.MAX_VALUE);
                response.addCookie(cookie);
            }
            return "redirect:/showEmployees";
        }
        return "redirect:/login";
    }

    @RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logout(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        Cookie endCookie = new Cookie("login", null);
        endCookie.setMaxAge(0);
        endCookie.setPath("/");
        response.addCookie(endCookie);
        System.out.println(endCookie.getName());
    if(session!=null)
            session.invalidate();
    return"redirect:/showEmployees";
}
}