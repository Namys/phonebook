package com.getjavajob.web06.phoneBook.ui.filter;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 * Created by ����� on 15.05.2016.
 */
@Controller
public class commonController {
    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String index() {
        return "redirect:/pageEmployees?offset=0&limit=10";
    }
}
