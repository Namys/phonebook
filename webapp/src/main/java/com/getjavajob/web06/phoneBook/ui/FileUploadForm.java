package com.getjavajob.web06.phoneBook.ui;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by ����� on 27.04.2016.
 */

public class FileUploadForm {
    private MultipartFile file;

    public MultipartFile getFile() {
        return file;
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }
}
