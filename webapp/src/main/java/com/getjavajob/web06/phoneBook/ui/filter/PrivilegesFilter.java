package com.getjavajob.web06.phoneBook.ui.filter;

import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.annotation.Resource;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Properties;

/**
 * Created by ����� on 27.12.2015.
 */

public class PrivilegesFilter implements HandlerInterceptor {

    @Resource(name = "usersProperties")
    private Properties properties;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        Cookie[] cookies = request.getCookies();
        HttpSession httpSession = request.getSession();
        if (httpSession != null) {
            Object login = httpSession.getAttribute("login");
            if (login != null) {
                return true;
            }
        }
        Boolean isRemember = false;
        String email = null;
        System.out.println(httpSession);
        for (int i = 0; i <cookies.length; i++) {
            Cookie cookie = cookies[i];

            if (cookie.getName().equals("login")) {
                String[] split =cookies[i].getValue().split(":");
                String pass = properties.getProperty(split[0]);
                System.out.println(split[0]);
                System.out.println(split[1]);
                if (pass != null && pass.equals(split[1])) {
                    isRemember = true;
                    email = split[0];
                }
            }
        }
        if (httpSession != null && isRemember) {
            httpSession.setAttribute("login", email);
            return true;
        }
        String id = request.getContextPath() + "/showEmployee?id=" + request.getParameter("id");
        response.sendRedirect(id);
        return false;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {

    }
}
