/**
 * Created by Намыс on 19.05.2016.
 */
handlerRemove();
$(function () {
    $("#datepicker").datepicker();
});
$(".phone").mask("(999) 999-9999");
String.prototype.replaceAll = function(search, replaceAll){
    return this.split(search).join(replaceAll);
}
function handlerRemove() {
    $( "body" ).off( "click", ".deleteForm");
    $(".deleteForm").click(function () {
        p = $(this);
        for (var j = 0; j < this.value; j++) {
            p = p.parent();
        }
        p.remove();
    });
}
var phoneText ="\<tr id=\"phone${phone.id}\">\<td>\<button type=\"button\" class=\"deleteForm\" value=\"2\">Удалить телефон\</button>\<label>Номер\<input class=\"form-control\" required class=\"phone\" name=\"phones[${status.index}].number\" value=\"\"/>\</label>\</td>\</tr>";
function addPhone() {
    var phone = phoneText.replaceAll("${status.index}", newPhone);
    phone = phone.replaceAll("${phone.id}", newPhone);
    $("#phones").append(phone);
    newPhone++;
    handlerRemove()
}
var addressText = "\<tr id=\"address${address.id}\">\<td>\<div>\<label>\<input class=\"form-control\" required name=\"addresses[${status.index}].country\"value=\"\"/>\</label>\<label>\<input class=\"form-control\" required name=\"addresses[${status.index}].city\"value=\"\"/>\</label>\<label>\<input class=\"form-control\" required name=\"addresses[${status.index}].street\"value=\"\"/>\</label>\<label>\<input class=\"form-control\" required name=\"addresses[${status.index}].house\"value=\"\"/>\</label>\<label>\<input class=\"form-control\" required name=\"addresses[${status.index}].room\"value=\"\"/>\</label>\<label>\<input class=\"form-control\" required name=\"addresses[${status.index}].address_index\"value=\"\"/>\</label>\<label>\<select name=\"addresses[${status.index}].isHome\">\<c:if test=\"${address.home}\">\<option selected value=\"true\">Домашний\</option>\<option value=\"false\">Рабочий\</option>\</c:if>\<c:if test=\"${!address.home}\">\<option value=\"true\">Домашний\</option>\<option selected value=\"false\">Рабочий\</option>\</c:if>\</select>\</label>\<button type=\"button\" class=\"deleteForm\" value=\"3\">Удалить адресс\</button>\</div>\</td>\</tr>";
function addAddress() {
    var adr = addressText.replaceAll(new RegExp("${status.index}",'g'), newAddress);
    adr = adr.replaceAll(new RegExp("${address.id}",'g'), newAddress);
    $("#addresses").append(adr);
    newAddress++;
    handlerRemove()
}
$("form").submit(function (event) {
    jConfirm('Can you confirm this?', 'Confirmation Dialog', function (r) {
        jAlert('Confirmed: ' + r, 'Confirmation Results');
    });

    $("span").text("Not valid!").show().fadeOut(1000);
    event.preventDefault();
});


