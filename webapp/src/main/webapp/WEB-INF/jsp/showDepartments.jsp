<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="header.jsp" %>

<div class="container">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Name</th>
            <th>Phones</th>
            <th>addresses</th>
            <th>BOSS</th>
        </tr>
        </thead>
        <c:forEach var="department" items="${departments}">
            <tr>
                <td><a href="${pageContext.request.contextPath}/updateDepartment?id=${department.id}" style="min-height: 5px;min-width: 5px">${department.name}</a></td>
                <td>
                    <c:forEach var="phone" items="${department.phones}">
                        <div>${phone.number}</div>
                    </c:forEach>
                </td>
                <td>
                    <c:forEach var="address" items="${department.addresses}">
                        <div>${address.id}</div>
                    </c:forEach>
                </td>
                <td>${department.chief.surname} ${department.chief.name} ${department.chief.family_name}</td>
            </tr>
        </c:forEach>
    </table>
</div>
<%@include file="footer.jsp" %>