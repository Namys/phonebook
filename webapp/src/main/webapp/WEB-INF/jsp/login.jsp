<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@include file="header.jsp"%>
<form:form role="form" action="${pageContext.request.contextPath}/doLogin" method="post">
  <div class="form-group">
    <label for="email">Email address:</label>
    <input name="email" type="email" class="form-control" id="email">
  </div>
  <div class="form-group">
    <label for="pwd">Password:</label>
    <input name="password" type="password" class="form-control" id="pwd">
  </div>
  <div class="checkbox">
    <label><input name="remember" type="checkbox"> Remember me</label>
  </div>
  <input type="submit">
</form:form>
<%@include file="footer.jsp"%>