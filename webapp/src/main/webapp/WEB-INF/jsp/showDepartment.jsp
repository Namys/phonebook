<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="header.jsp" %>
<script type="text/javascript" src="/resources/js/jquery.maskedinput.min.js"></script>

<div class="container">
    <h2>Департамент</h2>
    <form:form role="form" id="form" method="post" action="${pageContext.request.contextPath}/doUpdateDepartment"
               modelAttribute="departmentForm">
        <div class="form-group">
            <label for="name">Name</label>
            <input type="hidden" id="id" value="${department.id}">
            <input required type="text" class="form-control" id="name" value="${department.name}">
        </div>
        <div class="form-group">
            <label>
                <select required name="chief_id">
                    <c:if test="${department.chief==null}">
                        <option value="0" selected>none</option>
                        <c:forEach items="${employees}" var="employee_par">
                            <option value="${employee_par.id}">${employee_par.name}</option>

                        </c:forEach>
                    </c:if>
                    <c:if test="${department.chief!=null}">
                        <option selected
                                value="${department.chief.id}">${department.chief.name}</option>
                        <c:forEach items="${employees}" var="employee_par">
                            <c:if test="${department.chief.id!=employee_par.id}">
                                <option value="${employee_par.id}">${employee_par.name}</option>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </select>
            </label>
        </div>
        <div class="form-group">
            <label>Телефоны</label>
            <table id="phones">
                <tr>
                    <td>
                        <button onclick="addPhone()" type="button">Добвить телефон</button>
                    </td>
                </tr>
                <c:forEach items="${department.phones}" var="phone" varStatus="status">
                    <tr id="phone${phone.id}">
                        <td>
                            <button type="button" class="deleteForm" value="2">Удалить телефон</button>
                            <label>
                                Номер
                                <input class="form-control" type="hidden" name="phones[${status.index}].id"
                                       value="${phone.id}"/>
                                <input class="form-control" required class="phone"
                                       name="phones[${status.index}].number"
                                       value="${phone.number}"/>
                            </label>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <div class="form-group">
            <table id="addresses">
                <tr>
                    <td>
                        <button onclick="addAddress()" type="button">Добвить адрес</button>
                    </td>
                </tr>
                <c:forEach items="${department.addresses}" var="address" varStatus="status">
                    <tr id="address${address.id}">
                        <td>
                            <div>
                                <label>
                                    <input class="form-control" required type="hidden"
                                           name="addresses[${status.index}].id"
                                           value="${address.id}"/>
                                </label>
                                <label>
                                    <input class="form-control" required
                                           name="addresses[${status.index}].country"
                                           value="${address.country}"/>
                                </label>
                                <label>
                                    <input class="form-control" required name="addresses[${status.index}].city"
                                           value="${address.city}"/>
                                </label>
                                <label>
                                    <input class="form-control" required
                                           name="addresses[${status.index}].street"
                                           value="${address.street}"/>
                                </label>
                                <label>
                                    <input class="form-control" required name="addresses[${status.index}].house"
                                           value="${address.house}"/>
                                </label>
                                <label>
                                    <input class="form-control" required name="addresses[${status.index}].room"
                                           value="${address.room}"/>
                                </label>
                                <label>
                                    <input class="form-control" required
                                           name="addresses[${status.index}].address_index"
                                           value="${address.address_index}"/>
                                </label>
                                <label>
                                    <select name="addresses[${status.index}].isHome">
                                        <c:if test="${address.home}">
                                            <option selected value="true">Домашний</option>
                                            <option value="false">Рабочий</option>
                                        </c:if>
                                        <c:if test="${!address.home}">
                                            <option value="true">Домашний</option>
                                            <option selected value="false">Рабочий</option>
                                        </c:if>
                                    </select>
                                </label>
                                <button type="button" class="deleteForm" value="3">Удалить адресс</button>
                            </div>
                        </td>
                    </tr>
                </c:forEach>
            </table>
        </div>
        <button type="submit" class="btn btn-default">Submit</button>
    </form:form>
</div>
<script>
    var newPhone = ${department.phones.size()}+1;
    var newAddress = ${department.addresses.size()}+1;
</script>
<script type="text/javascript" src="/resources/js/formChange.js" charset="UTF-8"></script>
<%@include file="footer.jsp" %>