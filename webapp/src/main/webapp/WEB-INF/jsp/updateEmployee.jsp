<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@include file="header.jsp" %>
<script type="text/javascript" src="/resources/js/jquery.maskedinput.min.js"></script>
<c:if test="${show}">
    <style>
        button {
            display: none;
        }
        input[type=submit] {
            display: none;
        }
    </style>
    <script>
        $(document).ready(function(){
            disableElements("input");
            disableElements("select")
        });
        function disableElements(text) {
            var elements = $(text);
            for (var j = 0; j < elements.length; j++) {
                elements[j].disabled = true;
            }
        }
    </script>
</c:if>

<div class="container">
    <form:form role="form" id="form" method="post" action="${pageContext.request.contextPath}/doUpdate"
               modelAttribute="employeeForm">
        <table class="table-bordered">
            <tr>
                <th>Сотрудник номер ${employee.id}<br>

                    <div><a href="${pageContext.request.contextPath}/removeEmployee?employee_id=${employee.id}">Удалить
                        сотрудника</a></div>
                    <div><a href="${pageContext.request.contextPath}/show?id=${employee.id}">Сменить фото
                        сотрудника</a></div>
                    <img id="img" src="data:image/png;base64,${img}" alt="" width="150" height="150">
                </th>
                <th>

            </tr>
            <tr>
                <td>ФИО:
                    <br>
                    <label>
                        <input class="form-control" type="hidden" value="${employee.id}" name="id"/>
                        <input class="form-control" required type="text" name="surname" value="${employee.surname}"/>
                        <input class="form-control" required type="text" name="name" value="${employee.name}"/>
                        <input class="form-control" required type="text" name="family_name"
                               value="${employee.family_name}"/>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Email:
                        <br>
                        <input class="form-control" required type="email" name="email" value="${employee.email}"/>
                    </label>
                <td>
            </tr>
            <tr>
                <td>
                    <label for="datepicker">Дата рождения:</label><input class="form-control" required readonly
                                                                         type="text"
                                                                         name="bornDate"
                                                                         value='<fmt:formatDate value="${employee.bornDate}" pattern="MM/dd/yyyy"/>'
                                                                         id="datepicker"/>
                <td>
            </tr>
            <tr>
                <td>
                    <label>ICQ:
                        <br>
                        <input class="form-control" required type="text" name="icq" value="${employee.icq}"/>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>SKYPE
                        <input class="form-control" type="text" name="skype" value="${employee.skype}"/>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    Other:
                    <label>
                        <input class="form-control" type="text" name="other" value="${employee.other}"/>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>Телефоны</label>
                    <table id="phones">
                        <tr>
                            <td>
                                <button onclick="addPhone()" type="button">Добвить телефон</button>
                            </td>
                        </tr>
                        <c:forEach items="${employee.phones}" var="phone" varStatus="status">
                            <tr id="phone${phone.id}">
                                <td>
                                    <button type="button" class="deleteForm" value="2">Удалить телефон</button>
                                    <label>
                                        Номер
                                        <input class="form-control" type="hidden" name="phones[${status.index}].id"
                                               value="${phone.id}"/>
                                        <input class="form-control" required class="phone"
                                               name="phones[${status.index}].number"
                                               value="${phone.number}"/>
                                    </label>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="addresses">
                        <tr>
                            <td>
                                <button onclick="addAddress()" type="button">Добвить адрес</button>
                            </td>
                        </tr>
                        <c:forEach items="${employee.addresses}" var="address" varStatus="status">
                            <tr id="address${address.id}">
                                <td>
                                    <div>
                                        <label>
                                            <input class="form-control" required type="hidden"
                                                   name="addresses[${status.index}].id"
                                                   value="${address.id}"/>
                                        </label>
                                        <label>
                                            <input class="form-control" required
                                                   name="addresses[${status.index}].country"
                                                   value="${address.country}"/>
                                        </label>
                                        <label>
                                            <input class="form-control" required name="addresses[${status.index}].city"
                                                   value="${address.city}"/>
                                        </label>
                                        <label>
                                            <input class="form-control" required
                                                   name="addresses[${status.index}].street"
                                                   value="${address.street}"/>
                                        </label>
                                        <label>
                                            <input class="form-control" required name="addresses[${status.index}].house"
                                                   value="${address.house}"/>
                                        </label>
                                        <label>
                                            <input class="form-control" required name="addresses[${status.index}].room"
                                                   value="${address.room}"/>
                                        </label>
                                        <label>
                                            <input class="form-control" required
                                                   name="addresses[${status.index}].address_index"
                                                   value="${address.address_index}"/>
                                        </label>
                                        <label>
                                            <select name="addresses[${status.index}].isHome">
                                                <c:if test="${address.home}">
                                                    <option selected value="true">Домашний</option>
                                                    <option value="false">Рабочий</option>
                                                </c:if>
                                                <c:if test="${!address.home}">
                                                    <option value="true">Домашний</option>
                                                    <option selected value="false">Рабочий</option>
                                                </c:if>
                                            </select>
                                        </label>
                                        <button type="button" class="deleteForm" value="3">Удалить адресс</button>
                                    </div>
                                </td>
                            </tr>
                        </c:forEach>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <select name="department_id">
                            <option selected value="${employee.department.id}">${employee.department.name}</option>
                            <c:forEach items="${departments}" var="department1">
                                <c:if test="${employee.department.id!=department1.id}">
                                    <option value="${department1.id}">${department1.name}</option>
                                </c:if>
                            </c:forEach>
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <label>
                        <select name="chief_id">
                            <c:if test="${employee.chief==null}">
                                <option value="0" selected>none</option>
                                <c:forEach items="${employees}" var="employee_par">
                                    <option value="${employee_par.id}">${employee_par.name}</option>

                                </c:forEach>
                            </c:if>
                            <c:if test="${employee.chief!=null}">
                                <option selected
                                        value="${employee.chief.id}">${employee.chief.name}</option>
                                <c:forEach items="${employees}" var="employee_par">
                                    <c:if test="${employee.chief.id!=employee_par.id&&employee.id!=employee_par.id}">
                                        <option value="${employee_par.id}">${employee_par.name}</option>
                                    </c:if>
                                </c:forEach>
                            </c:if>
                        </select>
                    </label>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="form-control" type="submit">
                </td>
            </tr>
        </table>
        <br/>
    </form:form>
</div>
<script>
    var newPhone = ${employee.phones.size()}+1;
    var newAddress = ${employee.addresses.size()}+1;
</script>
<script type="text/javascript" src="/resources/js/formChange.js" charset="UTF-8"></script>
<%@include file="footer.jsp" %>