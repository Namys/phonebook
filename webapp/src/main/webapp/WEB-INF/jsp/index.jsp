<%@ page import="org.springframework.web.context.WebApplicationContext" %>
<%@ page import="org.springframework.web.context.support.WebApplicationContextUtils" %>
<%@ page import="com.getjavajob.web06.phoneBook.dao.impl.EmployeeDao" %>
<%@ page import="com.getjavajob.web06.phoneBook.dao.impl.PhoneDao" %>
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@include file="header.jsp" %>
<div class="ui-widget">
    <form id="search" action="${pageContext.request.contextPath}/update" method="get">
        <label for="employeeName">Поиск</label><input id="employeeName"/>
        <label for="id"></label><input required type="text" id="id" name="id" style="display: none"/>
        <input type="submit">
    </form>
</div>

<div class="panel-container">
    <table class="table table-bordered">
        <thead>
        <tr>
            <th>Firstname</th>
            <th>Lastname</th>
            <th>Middlename</th>
            <th>Phones</th>
            <th>Email</th>
            <th>Born date</th>
            <th>ICQ</th>
            <th>Skype</th>
            <th>Other</th>
            <th>BOSS</th>
            <th>LINK</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="employee" items="${employees}">
            <tr>
                <td>${employee.surname}</td>
                <td>${employee.name}</td>
                <td>${employee.family_name}</td>
                <td>
                    <c:forEach var="phone" items="${employee.phones}">
                        <div>${phone.number}</div>
                    </c:forEach>
                </td>
                <td>${employee.email}</td>
                <td>${employee.bornDate}</td>
                <td>${employee.icq}</td>
                <td>${employee.skype}</td>
                <td>${employee.other}</td>
                <td>
                    id:${employee.chief.id} ${employee.chief.surname} ${employee.chief.name} ${employee.chief.family_name}</td>
                <td><a href='<c:url value="/update?id=${employee.id}" />'>${employee.name}</a></td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    <c:if test="${paginationID}">
        <ul class="pagination">
            <c:if test="${pageNumber>1}">
                <li>
                    <a href="${pageContext.request.contextPath}/pageEmployees?start=${(pageNumber-2)*10+1}&end=${(pageNumber-1)*10}">Previous</a>
                </li>
            </c:if>
            <c:if test="${!maxPage}">
                <li>
                    <a href="${pageContext.request.contextPath}/pageEmployees?start=${pageNumber*10+1}&end=${(pageNumber+1)*10}">Next</a>
                </li>
            </c:if>
        </ul>
    </c:if>
    <c:if test="${pagination}">
        <ul class="pagination">
            <c:if test="${pageNumber>1}">
                <li>
                    <a href="${pageContext.request.contextPath}/pageEmployees?offset=${(pageNumber-2)*limit}&limit=${limit}">Previous</a>
                </li>
            </c:if>
            <c:if test="${!maxPage}">
                <li>
                    <a href="${pageContext.request.contextPath}/pageEmployees?offset=${pageNumber*limit}&limit=${limit}">Next</a>
                </li>
            </c:if>
        </ul>
    </c:if>
</div>

<script>
    $(document).ready(function () {
        $("#employeeName").autocomplete({
            source: function (request, response) {
                $.ajax({
                    url: '<c:url value="/getEmployees" />',
                    data: {
                        filter: request.term
                    },
                    method: "POST",
                    success: function (data) {
                        response($.map(data, function (e, i) {
                            return {value: e, label: e, attr: i}
                        }));
                    },
                    error: function () {
                        console.log('Error');
                    }
                });
            },
            select: function (event, ui) {
                $("#id").val(ui.item.attr);
            },
            minLength: 2
        });
    });
</script>
<%@include file="footer.jsp" %>